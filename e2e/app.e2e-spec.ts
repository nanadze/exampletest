import { ExampleTestPage } from './app.po';

describe('example-test App', function() {
  let page: ExampleTestPage;

  beforeEach(() => {
    page = new ExampleTestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
